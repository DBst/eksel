<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
        'css/site.css',
        'plugins/iziToast/css/iziToast.min.css',
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.6.0.min.js',
        'https://use.fontawesome.com/4d46885538.js',
        'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js',
        'plugins/iziToast/js/iziToast.min.js',
        'js/loader.js',
        'js/calculation.js',
        'js/features.js',
        'js/functions.js',
        'js/listeners.js',
        'js/site.js',
        'js/save.js',
        'js/select.js',
        'js/init.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public function init()
    {
        parent::init();
        // \Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
        //     'css' => [
        //         'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
        //     ],
        //     'js' => ['https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js']
        // ];
    }
}
