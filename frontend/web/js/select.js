var selectedCells = [];
let mousemove = false;

$(document).on('mousedown', '.cell', function(e){
    var mousedown_ = e;
    var this_ = this;
    var thisValue = $('#' + lastClicked).find('input').val();
    var position = $('#' + lastClicked).find('input')[0].selectionStart;
    mousemove = $(document).off('mousemove', '.cell').on('mousemove', '.cell', function(){
        if(mousedown_ !== false) {
            mousedown_.preventDefault();
            selectedCells = getCellsBetween($(this_).attr('id'), $(this).attr('id'));
            backlightCells(selectedCells);
            selectInit(mousedown_, this_, this, position, thisValue);
        }
    });
    $(document).off('mouseup', '.cell').on('mouseup', '.cell', function(e){
        mousedown_ = false;
        $(document).off('mousemove', '.cell');
    });
});
$(document).on('keydown', 'body', function(e){
    if(e.keyCode == 46) {
        if(selectedCells.length) {
            $.each(selectedCells, function(i, item){
                clearCell(item)
            });
        }
    }
});

function selectInit(e, this_, this__, position, thisValue)
{
    if($('#' + lastClicked).find('input').val().startsWith('=')) {
        $('#' + lastClicked).find('input').focus();
        $('#' + lastClicked).find('input').val(
            insertIntoText(thisValue, $(this_).attr('id') + ':' + $(this__).attr('id'), position)
        );
    }

}