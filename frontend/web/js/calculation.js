let refreshCache;
let cellsToRefresh;
let parentCells;
var childCells;

function calcCell(this_)
{
    if($(this_).val().startsWith('=')) {
        var command = $(this_).val().replace(' ', '');
        command = reCalcCell(command);
        command = emptyCell(command);
        if(isNaN(command)) {
            cellsQueue[$(this_).parent().attr('id')] = command;
        }
        try {
            $(this_).parent().find('.display-value').html(eval(command));
            $(this_).parent().find('.display-value').attr('value', eval(command));
        } catch(error) {
            $(this_).parent().find('.display-value').html('ERROR');
            $(this_).parent().find('.display-value').attr('value', 0);
            errorAlert();
        }
    } else {
        $(this_).parent().find('.display-value').html($(this_).val());
        $(this_).parent().find('.display-value').attr('value', $(this_).val());
    }
}

function addFunctions(command)
{
    command = SUM(command);
    command = AVG(command);

    return command;
}

function reCalcCell(command, count = 0)
{
    command = addFunctions(command);
    var commandCells = getCellsFromCommand(command);
    $.each(commandCells, function(i2, item2){
        command = command.replace(
            item2, $('#' + item2).find('.display-value').attr('value')
        );
    });
    $.each(cells, function(i, item){
        var values = $('#' + i + ' > input').val();
        command = command.replace(
            i, values
        ).replace('=', '');
    });
    var regExp = /[a-zA-Z]/g;
    if(count == 19) {
        command = emptyCell(command);
    }
    if(regExp.test(command) && count <= 20){
        return reCalcCell(command, count+1);
    }
    return command;
}

function emptyCell(command)
{
    var commandCells = getCellsFromCommand(command);
    $.each(commandCells, function(i, item){
        if(cellIsEmpty(item)) {
            command = command.replace(item, 0);
        }
    });
    return command;
}

function refreshChildCells(cell)
{
    getCells();
    cellsToRefresh = [];
    childCells = [];
    parentCells = [];
    getParents(cell);
    getChilds(cell);
    $.each(childCells, function(i, item){
        cellsToRefresh.push(item);
    });
    $.each(parentCells, function(i, item){
        cellsToRefresh.push(item);
    });
    refreshCells();
}

function refreshCells(count = 0)
{
    var lastRefreshCache = refreshCache;
    refreshCache = '';
    $.each(cellsToRefresh, function(i, cell){
        calcCell($('#' + cell).find('input'));
        refreshCache = refreshCache + $('#' + cell).find('input').val();
    });
    if(count < 20) {
        if(lastRefreshCache !== refreshCache) {
            refreshCells(count + 1);
        }
    }
    getCells();
}

function getParents(cell)
{
    try {
        parentCells.push(cell)
        let localCells = getCellsFromCommand($('#' + cell).find('input').val());
        $.each(localCells, function(i, cell){
            getParents(cell);
        });
    } catch(error) {
        errorAlert('Wykryto zależność cykliczną!');
    }
}

function getChilds(cell)
{
    try {
        $.each(cells, function(i, cell2){
            if(cell2.includes(cell) || getCellsFromCommand(cell2).includes(cell)) {
                if(typeof cellsToRefresh[i] === 'undefined') {
                    childCells.push(i);
                    getChilds(i);
                }
            }
        });
    } catch(error) {
        errorAlert();
    }
}