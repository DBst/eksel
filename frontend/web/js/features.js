let availableFeatures = ['SUM', 'AVG'];

function SUM(command)
{
    if(command.includes('SUM')) {
        var sum = command.split('SUM(')[1];
        sum = sum.split(')')[0];
        var cell1 = sum.split(':')[0];
        var cell2 = sum.split(':')[1];
        var cells = getCellsBetween(cell1, cell2)
        var total = 0;
        $.each(cells, function(i, item){
            if(isCell(item)) {
                if($('#' + item).find('.display-value').attr('value') !== $('#' + item).find('input').val()) {
                    $('#' + item).find('.display-value').attr('value', reCalcCell($('#' + item).find('input').val()));
                }
                total += parseFloat($('#' + item).find('.display-value').attr('value'));
            }
        })
        command = command.replace('SUM(' + sum + ')', total);
    }
    return command;
}

function AVG(command)
{
    if(command.includes('AVG')) {
        var sum = command.split('AVG(')[1];
        sum = sum.split(')')[0];
        var cell1 = sum.split(':')[0];
        var cell2 = sum.split(':')[1];
        var cells = getCellsBetween(cell1, cell2)
        var total = 0;
        var count = 0;
        $.each(cells, function(i, item){
            if(isCell(item)) {
                if($('#' + item).find('.display-value').attr('value') !== $('#' + item).find('input').val()) {
                    $('#' + item).find('.display-value').attr('value', $('#' + item).find('input').val());
                }
                total += parseFloat($('#' + item).find('.display-value').attr('value'));
                count++;
            }
        })
        total = total / count;
        command = command.replace('AVG(' + sum + ')', total);
    }
    return command;
}