var pressedCtrl = false;
var pressedLC = false;

$(document).on('keyup', '.cell > input', function(e){
    getCells();
    if(e.keyCode == 13) {
        // parseCell(this);
        changeSelectedInput($(this).parent().attr('id'), 'down');
        saveData();
        $('#command-line').val('');
    } else if(e.keyCode >= 48 && e.keyCode <= 57 || e.keyCode == 8) {
        
    }
    
    backlightCells(getCellsFromCommand($(this).val()));
    $('#command-line').val($(this).val());
    if(e.keyCode == 13) {
        $('#command-line').val('');
    }
    $('.backlight').removeClass('backlight');
});
$(document).on('keydown', '.cell > input', function(e){
    if(e.keyCode >= 37 && e.keyCode <= 40) {
        var prevent = false;
        if(e.keyCode == 37 && $(this).val() !== '') {
            if($(this)[0].selectionStart != 0) {
                prevent = true;
            }
        } else if(e.keyCode == 39 && $(this).val() !== '') {
            if($(this).val().length !== $(this)[0].selectionStart) {
                prevent = true;
            }
        }
        if(!prevent) {
            changeSelectedInput($(this).parent().attr('id'), (e.key).replace('Arrow', '').toLowerCase());
        }
    }
});
$(document).on('click', '.cell', function(){
    if(!pressedCtrl) {
        backlightCells(getCellsFromCommand($(this).find('input').val()));
        $('#command-line').val($(this).find('input').val());
        $('#command-line').attr('value', $(this).find('input').val());
    }
});
$(document).on('focusout', '.cell > input', function(){
    if(!pressedCtrl) {
        refreshChildCells($(this).parent().attr('id'));
        backlightCells([]);
        setTimeout(function(){
            if(!commandLineActive) {
                $('#command-line').val('');
            }
        },100);
        $(document).off('click', '#command-line').on('click', '#command-line', function(){
            var this_ = this;
            setTimeout(function(){
                backlightCells(getCellsFromCommand($(this_).val()));
            },100);
            $('#command-line').val($('#' + lastClicked).find('input').val());
            $('#command-line').attr('value', $('#' + lastClicked).find('input').val());
        });
        if(typeof lastClicked !== 'undefined') {
            calcCell($('#' + lastClicked).find('input'));
        }
        saveData();
    }
});
$(document).on('keyup', '#command-line', function(e){
    $('#' + lastClicked).find('input').val($(this).val());
    $('#' + lastClicked).find('.display-value').html($(this).val());
    $('#' + lastClicked).find('.display-value').attr('value', $(this).val());
    backlightCells(getCellsFromCommand($(this).val()));
    if(e.keyCode == 13) {
        saveData();
    }
});

$(document).on('click', '.cell > .display-value', function(){
    if(!pressedCtrl) {
        getCells();
        if(typeof lastClicked !== 'undefined') {
        }
        lastClicked = $(this).parent().attr('id');
        changeSelectedInput(lastClicked, 'none');
    }
});

$(document).ready(function(){
    init();
});

$(document).on('focusin', '#command-line, .cell > input', function(){
    commandLineActive = true;
});
$(document).on('focusout', '#command-line', function(){
    commandLineActive = false;
});






// $(document).on('keydown', 'body', function(e){
//     if(e.keyCode == 17) {
//         pressedCtrl = true;
//     }
// });
// $(document).on('keyup', 'body', function(e){
//     if(e.keyCode == 17) {
//         pressedCtrl = false;
//     }
// });
// $(document).on('click', '.cell', function(e){
    // if(pressedCtrl) {
        // if($('#' + lastClicked).find('input').val().startsWith('=')) {
        //     e.preventDefault();
        //     addSelectedCells(e, this);
        // }
    // }
// });

$(document).on('focusin', '.cell', function(){
    $(this).find('input')[0].selectionStart = 1000000;
});


//column resizer
function columnResize() {
    try {
        $(document).on('mousedown', '.column-resize', function(e){
            var mouseStartPosition = e.pageX;
            var this_ = this;
            var colWidth = Number.parseInt($(this_).parent().css('min-width'));
            $('body').css('cursor', 'col-resize');
            $(document).off('mousemove', 'body').on('mousemove', 'body', function(e){
                var posDifference = e.pageX - mouseStartPosition;
                var width = (colWidth + posDifference) >= 100 ? colWidth + posDifference : 100;
                $(this_).parent().css('min-width', width + 'px');
            });
            $(document).off('mouseup', 'body').on('mouseup', 'body', function(e){
                $(document).off('mousemove', 'body');
                $('body').css('cursor', '');
                saveData();
            });
        });
    } catch(error) {
        errorAlert();
    }
}