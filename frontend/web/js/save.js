var settings = {
    'colSizes': {}
};

function saveData()
{
    initSettings();
    clearTimeout(keyUpTimeout);
    keyUpTimeout = setTimeout(function(){
        ajaxRequest('/save-cells', {
            cells: JSON.stringify(cells),
            settings: JSON.stringify(settings),
        }).then(function(result){
            if(result.status == true) {

            } else {
                errorAlert(result.message);
            }
        });
    }, 1000);
}

function initSettings()
{
    //get column sizes
    $.each($('.column'), function(){
        if($(this).css('min-width') !== '100px') {
            settings.colSizes[$(this).attr('data-letter')] = $(this).css('min-width');
        }
    });
}