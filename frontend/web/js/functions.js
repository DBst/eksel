function getCellsBetween(cell1, cell2)
{
    var localCells = new Array();
    var cell1LetterIndex = Number.parseInt(alphabet.indexOf(cell1.charAt(0)));
    var cell1Number = Number.parseInt(cell1.replace(/\D/g,''));
    var cell2LetterIndex = Number.parseInt(alphabet.indexOf(cell2.charAt(0)));
    var cell2Number = Number.parseInt(cell2.replace(/\D/g,''));
    
    if(cell1LetterIndex <= cell2LetterIndex) {
        for(var i = cell1LetterIndex; i <= cell2LetterIndex; i++) {
            if(cell1Number >= cell2Number) {
                for(var i2 = cell2Number; i2 <= cell1Number; i2++) {
                    localCells.push(alphabet[i] + i2);
                }
            } else {
                for(var i2 = cell2Number; i2 >= cell1Number; i2--) {
                    localCells.push(alphabet[i] + i2);
                }
            }
        }
    } else {
        for(var i = cell1LetterIndex; i >= cell2LetterIndex; i--) {
            if(cell1Number >= cell2Number) {
                for(var i2 = cell2Number; i2 <= cell1Number; i2++) {
                    localCells.push(alphabet[i] + i2);
                }
            } else {
                for(var i2 = cell2Number; i2 >= cell1Number; i2--) {
                    localCells.push(alphabet[i] + i2);
                }
            }
        }
    }

    return localCells;
}

function cellIsBetween(cell, between1, between2)
{
    var localCells = getCellsBetween(between1, between2);
    if(localCells.includes(cell)) {
        return true;
    }
    return false;
}

function changeSelectedInput(cell, direction = 'none')
{
    refreshChildCells(cell);
    $('.cell > input').addClass('d-none');
    $('.display-value').removeClass('d-none');
    if(direction == 'right') {
        var nextCell = $('#' + alphabet[alphabet.indexOf(cell.charAt(0))+1] + (Number.parseInt(cell.replace(/[^0-9]/gi, '')) ));
    } else if(direction == 'left') {
        var nextCell = $('#' + alphabet[alphabet.indexOf(cell.charAt(0))-1] + (Number.parseInt(cell.replace(/[^0-9]/gi, ''))) );
    } else if(direction == 'up') {
        var nextCell = $('#' + cell.charAt(0) + (Number.parseInt(cell.replace(/[^0-9]/gi, ''))-1) );
    } else if(direction == 'down') {
        var nextCell = $('#' + cell.charAt(0) + (Number.parseInt(cell.replace(/[^0-9]/gi, ''))+1) );
    } else if(direction == 'none') {
        var nextCell = $('#' + cell.charAt(0) + (Number.parseInt(cell.replace(/[^0-9]/gi, ''))) );
    }
    nextCell.find('.display-value').addClass('d-none');
    nextCell.find('input').removeClass('d-none');
    nextCell.find('input').focus();
    lastClicked = $(nextCell).attr('id');
}

function getCellsFromCommand(command)
{
    var localCells = new Array();
    var args = command.replaceAll(/ |=|\(|\)|SUM|AVG/gi, '');
    args = args.split(/[\.+*;/\-_]/);
    $.each(args, function(i, item){
        if(item.includes(':')) {
            var between = item.split(':');
            $.each(getCellsBetween(between[0], between[1]), function(i, item){
                if(isCell(item)) {
                    localCells.push(item);
                }
            });
        } else {
            if(isCell(item)) {
                localCells.push(item);
            }
        }
    });
    return localCells;
}

function isCell(id)
{
    if(id != '') {
        if($('#' + id).length) {
            if($('#' + id).hasClass('cell')) {
                return true;
            }
        }
    }
    return false;
}

function cellIsEmpty(id)
{
    if(isCell(id)) {
        if($('#' + id).find('.display-value').attr('value').length) {
            return false;
        }
    }
    return true;
}

function backlightCells(localCells)
{
    $('.backlight').removeClass('backlight');
    $.each(localCells, function(i, cell){
        $('#' + cell).addClass('backlight')
    });
}

function multipleSelection(e, this_)
{
    // console.log(123)
} 

function addSelectedCells(e, this_)
{
    var lastClickedInput = $('#' + lastClicked).find('input')
    var lastSpecialChar = $(lastClickedInput).val().replace(/[!^\w\s]/gi, '');
    lastSpecialChar = lastSpecialChar.charAt(lastSpecialChar.length-1);
    if(lastSpecialChar == '=') {
        if($(lastClickedInput).val().length == 1) {
            lastSpecialChar = '';
        } else {
            lastSpecialChar = '+';
        }
    }
    $(lastClickedInput).val($(lastClickedInput).val() + lastSpecialChar + $(this_).attr('id'));
    backlightCells(getCellsFromCommand($(lastClickedInput).val()));
    $('#command-line').val($(lastClickedInput).val())
}

function insertIntoText(text, value, index)
{
    text = text.split('');
    var finalText = '';
    for(var i = 0; index > i; i++) {
        finalText = finalText + text[i];
    }
    finalText = finalText + value;
    for(var i = index; text.length > i; i++) {
        finalText = finalText + text[i];
    }
    return finalText;

}

function errorAlert(message = 'Spróbuj ponowanie za chwilę!')
{
    iziToast.error({
        id: 'error',
        title: 'Wystąpił problem!',
        message: message,
        position: 'topRight',
        transitionIn: 'fadeInDown'
    });
}

function ajaxRequest(url, data)
{
    data['_csrf-frontend'] = $('meta[name="csrf-token"]').attr('content');
    return $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: data
    });
}

function getCells()
{
    cells = {};
    $('.cell > input').each(function(){
        var value = $(this).val();
        if(value != "") {
            cells[$(this).parent().attr('id')] = value;
        }
    });
}

function clearCell(cell)
{
    $('#' + cell).find('input').val('');
    $('#' + cell).find('.display-value').attr('value', '');
    $('#' + cell).find('.display-value').html('');
    saveData();
}