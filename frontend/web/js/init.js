
function init()
{
    initCells();
    initAddons();
}

function initCells()
{
    cellsQueue = {};
    for (i = 0; i < 26; i++) {
        alphabet.push((i+10).toString(36).toUpperCase());
    }
    getCells();
    $.each(cells, function(i){
        calcCell($('#' + i).find('input'));
    });
    $.each(cellsQueue, function(i){
        calcCell($('#' + i).find('input'));
    });
    cellsQueue = {};
    $.each(cells, function(i, item){
        refreshChildCells(i);
    });
}

function initAddons()
{
    columnResize();
}