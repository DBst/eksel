<?php

/* @var $this yii\web\View */

    $this->title = 'Eksel';
    if (!empty($data['data'])) {
        $cells = json_decode($data['data']['cells'], true);
        $settings = json_decode($data['data']['settings'], true);
    }
?>

<table>
    <thead>
        <tr>
            <th scope="col"></th>
            <?php foreach ($alphabet as $letter): ?>
            <th scope="col" class="position-relative column"
                style="min-width: <?= !empty($settings) ? (!empty($settings['colSizes'][$letter]) ? $settings['colSizes'][$letter] : '100px') : '100px' ?>;"
                data-letter="<?=$letter?>">
                <?=$letter?>
                <div class="column-resize"></div>
            </th>
            <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 1; $i < 100; $i++): ?>
        <tr>
            <th>
                <?=$i?>
            </th>
            <?php foreach ($alphabet as $letter): ?>
            <td class="cell"
                id="<?=$letter?><?=$i?>">
                <div class="display-value"
                    value="<?= !empty($cells[$letter . $i]) ? (strpos($cells[$letter . $i], '=') == false ? $cells[$letter . $i] : '') : '' ?>">

                </div>
                <input type="text" class="d-none"
                    value="<?= !empty($cells[$letter . $i]) ? $cells[$letter . $i] : '' ?>">
            </td>
            <?php endforeach; ?>
        </tr>
        <?php endfor; ?>
    </tbody>
</table>