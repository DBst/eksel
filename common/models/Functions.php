<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class Functions extends Model
{
    public function getAlphabet()
    {
        $alphabet = [];
        foreach (range('A', 'Z') as $value) {
            $alphabet[] = $value;
        }
        return $alphabet;
    }

    public function getUserIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function getCells()
    {
        try {
            if (!empty(Yii::$app->getRequest()->getCookies()->getValue('user_hash'))) {
                $user_hash = Yii::$app->getRequest()->getCookies()->getValue('user_hash');
                $cells = \common\models\Cells::find()->where(['user_hash' => $user_hash])->one();
                if (!empty($cells)) {
                    return ['status' => true, 'data' => $cells];
                }
            }
            return ['status' => false, 'data' => [], 'message' => 'Podczas próby zapisu coś poszło nie tak! Spróbuj ponowanie za chwilę'];
        } catch (\Exception $a) {
            Yii::error($a);
            return ['status' => false, 'data' => [], 'message' => 'Podczas próby zapisu coś poszło nie tak! Spróbuj ponowanie za chwilę'];
        }
    }
}
