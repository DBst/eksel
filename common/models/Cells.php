<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cells".
 *
 * @property int $id
 * @property string|null $user_hash
 * @property string|null $cells
 * @property string|null $settings
 */
class Cells extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cells';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cells', 'settings'], 'string'],
            [['user_hash'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_hash' => 'User Hash',
            'cells' => 'Cells',
            'settings' => 'Settings',
        ];
    }
}
